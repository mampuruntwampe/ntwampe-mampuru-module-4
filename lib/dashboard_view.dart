import 'package:flutter/material.dart';
import 'package:mtn_module_3/profile.dart';

import 'abstract_widgets/primary_button.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title:const Text("Dashboard",style: TextStyle(color: Colors.white,fontSize: 25.0,fontWeight: FontWeight.bold))),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const DashboardView()),
                  );
                },
                child: Card(
                  elevation: 2,
                  child: SizedBox(
                    height: 250,
                    width: _width,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.note,size: 50),
                        Text("Take Notes",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
                      ],
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const DashboardView()),
                  );
                },
                child: Card(
                  elevation: 2,
                  child: SizedBox(
                    height: 250,
                    width: _width,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.calculate_sharp,size: 50),
                        Text("Calculate",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
                      ],
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const ProfileView()),
                  );
                },
                child: SizedBox(
                  height: 250,
                  width: _width,
                  child: Card(
                    elevation: 2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(Icons.account_circle,size: 50,),
                        Text("Edit Profile",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.android),
        onPressed: () {

        },
      ),
    );
  }
}
