
import 'package:flutter/material.dart';
import 'package:mtn_module_3/signup_view.dart';

import 'abstract_widgets/primary_button.dart';
import 'abstract_widgets/primary_textfield.dart';
import 'dashboard_view.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Login",style: TextStyle(color: Colors.white,fontSize: 25.0,fontWeight: FontWeight.bold)),),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Card(
            elevation: 2,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                 Column(
                   children: [
                     const SizedBox(
                       height: 50,
                     ),
                     const Text("Welcome Back",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
                     const SizedBox(
                       height: 50,
                     ),
                     TextfieldWidget(label: "Username/Email",controller: nameController,textInputType: TextInputType.text,),
                     const SizedBox(
                       height: 20,
                     ),
                     TextfieldWidget(label: "Password",controller: nameController,textInputType: TextInputType.text,),
                     const SizedBox(
                       height: 50,
                     ),
                   ],
                 ),
                 Column(
                   crossAxisAlignment: CrossAxisAlignment.center,
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                   children: [
                     PrimaryButton(buttonName: "Login", onTap: (){
                       Navigator.push(
                         context,
                         MaterialPageRoute(builder: (context) => const DashboardView()),
                       );
                     }),
                     const SizedBox(
                       height: 20.0,
                     ),
                     TextButton(
                       style: const ButtonStyle(
                         // foregroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                       ),
                       onPressed: () {
                         Navigator.push(
                           context,
                           MaterialPageRoute(builder: (context) => const SignUpView()),
                         );
                       },
                       child: const Text('Sign Up',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                     ),
                   ],
                 ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
