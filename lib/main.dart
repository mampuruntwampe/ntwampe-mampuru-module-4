// import 'package:flutter/material.dart';
//
// import 'login_view.dart';
//
// void main() {
//   runApp(const MyApp());
// }
//
// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);
//
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.teal,
//       ),
//       home: const LoginView(),
//     );
//   }
// }

import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:mtn_module_3/login_view.dart';
import 'package:page_transition/page_transition.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Memo',
        debugShowCheckedModeBanner: false,
        darkTheme: ThemeData.dark(
        ),
        theme: ThemeData(
          primarySwatch: Colors.teal,
          textButtonTheme: TextButtonThemeData(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            )
          ),
          floatingActionButtonTheme: const FloatingActionButtonThemeData(
            backgroundColor: Colors.cyan,
          ),
        ),
        home: AnimatedSplashScreen(
            duration: 3000,
            splash: Center(child: Lottie.asset("assets/splash.json",height: 500,width: 500,fit: BoxFit.cover,)),
            nextScreen: const LoginView(),
            splashTransition: SplashTransition.slideTransition,
            ));
  }
}

