
import 'package:flutter/material.dart';
import 'package:mtn_module_3/dashboard_view.dart';

import 'abstract_widgets/primary_button.dart';
import 'abstract_widgets/primary_textfield.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  final nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Edit Profile"),),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Card(
            elevation: 2,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Image.asset("assets/user.png",width: 100,height: 100,),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextfieldWidget(label: "Full Name",controller: nameController,textInputType: TextInputType.text,),
                  const SizedBox(
                    height: 20,
                  ),
                  TextfieldWidget(label: "Username",controller: nameController,textInputType: TextInputType.text,),
                  const SizedBox(
                    height: 20,
                  ),
                  TextfieldWidget(label: "Email",controller: nameController,textInputType: TextInputType.emailAddress,),
                  const SizedBox(
                    height: 20,
                  ),
                  TextfieldWidget(label: "Password",controller: nameController,textInputType: TextInputType.text,),
                  const SizedBox(
                    height: 20,
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      PrimaryButton(buttonName: "Update", onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const DashboardView()),
                        );
                      }),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
