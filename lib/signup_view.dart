
import 'package:flutter/material.dart';
import 'package:mtn_module_3/dashboard_view.dart';
import 'package:mtn_module_3/login_view.dart';

import 'abstract_widgets/primary_button.dart';
import 'abstract_widgets/primary_textfield.dart';

class SignUpView extends StatefulWidget {
  const SignUpView({Key? key}) : super(key: key);

  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  final nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Sign Up"),),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  const Text("Hey! Stranger",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
                  const SizedBox(
                    height: 30,
                  ),
                  TextfieldWidget(label: "Full Name",controller: nameController,textInputType: TextInputType.text,),
                  const SizedBox(
                    height: 20,
                  ),
                  TextfieldWidget(label: "Username",controller: nameController,textInputType: TextInputType.text,),
                  const SizedBox(
                    height: 20,
                  ),
                  TextfieldWidget(label: "Email",controller: nameController,textInputType: TextInputType.text,),
                  const SizedBox(
                    height: 20,
                  ),
                  TextfieldWidget(label: "Password",controller: nameController,textInputType: TextInputType.text,),
                  const SizedBox(
                    height: 20,
                  ),
                  TextfieldWidget(label: "Confirm Password",controller: nameController,textInputType: TextInputType.text,),
                  const SizedBox(
                    height: 50,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      PrimaryButton(buttonName: "Sign Up",width: 250, onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const DashboardView()),
                        );
                      }),
                    ],
                  ),
                  TextButton(
                    style: const ButtonStyle(
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const LoginView()),
                      );
                    },
                    child: const Text('Login',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                  ),
                  const SizedBox(height: 10,)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
